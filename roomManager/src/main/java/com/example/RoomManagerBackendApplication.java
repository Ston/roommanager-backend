package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoomManagerBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoomManagerBackendApplication.class, args);
	}
}
